package gusl.auth.repository;

import gusl.auth.model.roles.RoleDO;
import gusl.model.dao.DAOInterface;
import org.jvnet.hk2.annotations.Contract;

@Contract
public interface RoleDAO extends DAOInterface<RoleDO> {
}
