package gusl.auth.repository;

import gusl.auth.model.geofence.GeoFenceDO;
import gusl.model.dao.DAOInterface;
import org.jvnet.hk2.annotations.Contract;

@Contract
public interface GeoFenceDAO extends DAOInterface<GeoFenceDO> {
}
