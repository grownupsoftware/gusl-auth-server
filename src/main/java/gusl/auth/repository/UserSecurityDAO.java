package gusl.auth.repository;

import gusl.auth.model.security.UserSecurityDO;
import gusl.model.dao.DAOInterface;
import org.jvnet.hk2.annotations.Contract;

@Contract
public interface UserSecurityDAO extends DAOInterface<UserSecurityDO> {

}
