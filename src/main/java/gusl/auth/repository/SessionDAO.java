package gusl.auth.repository;

import gusl.auth.model.session.SessionDO;
import gusl.auth.model.session.SessionStatus;
import gusl.core.exceptions.GUSLErrorException;
import gusl.model.dao.DAOInterface;
import org.jvnet.hk2.annotations.Contract;

import java.util.List;

@Contract
public interface SessionDAO extends DAOInterface<SessionDO> {

    List<SessionDO> getAllForUser(String userId) throws GUSLErrorException;

    SessionDO updateStatus(String id, SessionStatus newStatus) throws GUSLErrorException;

}
