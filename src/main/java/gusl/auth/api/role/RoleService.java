package gusl.auth.api.role;

import gusl.annotations.form.page.PagedDAOResponse;
import gusl.auth.model.roles.RoleDO;
import gusl.auth.model.roles.RolesDO;
import gusl.core.exceptions.GUSLErrorException;
import gusl.model.role.NodeRolesNotificationDO;
import gusl.model.role.NodeRolesRequestDO;
import gusl.model.utils.SimpleStringRequestDTO;
import gusl.query.QueryParams;
import org.jvnet.hk2.annotations.Contract;

import javax.ws.rs.container.AsyncResponse;

@Contract
public interface RoleService {

    PagedDAOResponse getAllPaged(QueryParams request) throws GUSLErrorException;

    RolesDO getAll() throws GUSLErrorException;

    RoleDO get(SimpleStringRequestDTO requestDo) throws GUSLErrorException;

    RoleDO insert(String username, RoleDO contents) throws GUSLErrorException;

    RoleDO update(String username, RoleDO contents) throws GUSLErrorException;

    void updateNodeRoles(AsyncResponse asyncResponse, NodeRolesNotificationDO request);

    void getNodeRoles(AsyncResponse asyncResponse, NodeRolesRequestDO request);
}

    
