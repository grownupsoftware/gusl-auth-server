package gusl.auth.api.role.cache;

import gusl.cache.AbstractIdentifiableStringCache;
import gusl.model.role.NodeRolesNotificationDO;
import org.jvnet.hk2.annotations.Service;

@Service
public class NodeRoleCentralCacheImpl extends AbstractIdentifiableStringCache<NodeRolesNotificationDO> implements NodeRoleCentralCache {

}
