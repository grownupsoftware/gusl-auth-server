package gusl.auth.api.role.cache;

import gusl.cache.KeyDoCache;
import gusl.model.role.NodeRolesNotificationDO;
import org.jvnet.hk2.annotations.Contract;

@Contract
public interface NodeRoleCentralCache extends KeyDoCache<String, NodeRolesNotificationDO> {
    void add(NodeRolesNotificationDO request);

}
