package gusl.auth.api.role;

import gusl.auth.model.roles.RoleDO;
import gusl.core.exceptions.GUSLErrorException;
import gusl.model.dataservice.dtos.AuditableDTO;
import gusl.model.role.NodeRolesNotificationDO;
import gusl.model.role.NodeRolesRequestDO;
import gusl.model.utils.SimpleStringRequestDTO;
import gusl.node.resources.AbstractBaseResource;
import gusl.query.QueryParams;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.validation.Valid;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;

import static gusl.node.transport.HttpUtils.resumeAsyncOK;

@Singleton
@Path("role/v1")
@PermitAll
public class RoleResource extends AbstractBaseResource {

    @Inject
    private RoleService theService;

    @POST
    @Path("paged")
    public void getAllPaged(@Suspended final AsyncResponse asyncResponse, final QueryParams request) throws GUSLErrorException {
        resumeAsyncOK(asyncResponse, theService.getAllPaged(request));
    }

    @POST
    @Path("query-params")
    public void getQueryParams(@Suspended final AsyncResponse asyncResponse, final QueryParams request) throws GUSLErrorException {
        resumeAsyncOK(asyncResponse, theService.getAllPaged(request));
    }

    @POST
    @Path("all")
    public void getAll(@Suspended final AsyncResponse asyncResponse) throws GUSLErrorException {
        resumeAsyncOK(asyncResponse, theService.getAll());
    }

    @POST
    @Path("get")
    public void get(@Suspended final AsyncResponse asyncResponse, SimpleStringRequestDTO requestDo) throws GUSLErrorException {
        resumeAsyncOK(asyncResponse, theService.get(requestDo));
    }

    @POST
    @Path("insert")
    public void insert(@Suspended final AsyncResponse asyncResponse, @Valid AuditableDTO<RoleDO> dto) throws GUSLErrorException {
        resumeAsyncOK(asyncResponse, theService.insert(dto.getUsername(), dto.getContents()));
    }

    @POST
    @Path("update")
    public void update(@Suspended final AsyncResponse asyncResponse, @Valid AuditableDTO<RoleDO> dto) throws GUSLErrorException {
        resumeAsyncOK(asyncResponse, theService.update(dto.getUsername(), dto.getContents()));
    }

    @POST
    @Path("update-node-roles")
    public void updateNodeRoles(@Suspended final AsyncResponse asyncResponse, NodeRolesNotificationDO request){
        theService.updateNodeRoles(asyncResponse,request);
    }

    @POST
    @Path("get-node-roles")
    public void getNodeRoles(@Suspended final AsyncResponse asyncResponse, NodeRolesRequestDO request){
       theService.getNodeRoles(asyncResponse,request);
    }



}
    
