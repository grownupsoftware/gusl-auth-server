package gusl.auth.api.role;

import gusl.annotations.form.page.PagedDAOResponse;
import gusl.auth.api.role.cache.NodeRoleCentralCache;
import gusl.auth.model.roles.RoleCacheEvent;
import gusl.auth.model.roles.RoleDO;
import gusl.auth.model.roles.RolesDO;
import gusl.auth.repository.RoleDAO;
import gusl.core.exceptions.GUSLErrorException;
import gusl.model.cache.CacheAction;
import gusl.model.role.NodeRolesNotificationDO;
import gusl.model.role.NodeRolesNotificationEvent;
import gusl.model.role.NodeRolesRequestDO;
import gusl.model.role.NodeRolesResponseDO;
import gusl.model.success.SuccessResponseDO;
import gusl.model.utils.SimpleStringRequestDTO;
import gusl.node.service.AbstractNodeService;
import gusl.query.QueryParams;
import lombok.CustomLog;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;
import javax.ws.rs.container.AsyncResponse;

import static gusl.node.transport.HttpUtils.resumeAsyncOK;

@CustomLog
@Service
public class RoleServiceImpl extends AbstractNodeService implements RoleService {

    @Inject
    private RoleDAO theRoleDAO;

    @Inject
    private NodeRoleCentralCache theNodeRoleCentralCache;

    @Override
    public PagedDAOResponse getAllPaged(QueryParams request) throws GUSLErrorException {
        return theRoleDAO.getAllPaged(request);
    }

    @Override
    public RolesDO getAll() throws GUSLErrorException {
        return RolesDO.builder().roles(theRoleDAO.getAll()).build();
    }

    @Override
    public RoleDO get(SimpleStringRequestDTO requestDo) throws GUSLErrorException {
        return theRoleDAO.findRecord(requestDo.getId());
    }

    @Override
    public RoleDO insert(String username, RoleDO contents) throws GUSLErrorException {
        theRoleDAO.insert(contents);
        broadcastEventReportException(new RoleCacheEvent(CacheAction.ADD, contents));
        return contents;
    }

    @Override
    public RoleDO update(String username, RoleDO contents) throws GUSLErrorException {
        final RoleDO latest = theRoleDAO.update(contents);
        broadcastEventReportException(new RoleCacheEvent(CacheAction.UPDATE, latest));
        return latest;
    }

    @Override
    public void updateNodeRoles(AsyncResponse asyncResponse, NodeRolesNotificationDO request) {
        theNodeRoleCentralCache.add(request);
        broadcastEventReportException(NodeRolesNotificationEvent.builder().content(request).build());
        resumeAsyncOK(asyncResponse, SuccessResponseDO.success());
    }

    @Override
    public void getNodeRoles(AsyncResponse asyncResponse, NodeRolesRequestDO request) {
        resumeAsyncOK(asyncResponse, NodeRolesResponseDO.builder().nodeRoles(theNodeRoleCentralCache.getAll()).build());
    }

}
