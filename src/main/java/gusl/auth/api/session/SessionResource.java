package gusl.auth.api.session;

import gusl.auth.model.session.SessionGetRequestDO;
import gusl.auth.model.session.SessionSelectAllDO;
import gusl.auth.model.session.SessionStatusChangeRequestDO;
import gusl.core.exceptions.GUSLErrorException;
import gusl.node.resources.AbstractBaseResource;
import gusl.query.QueryParams;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.validation.Valid;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;

import static gusl.node.transport.HttpUtils.resumeAsyncOK;

@Singleton
@Path("/session/v1")
@PermitAll
public class SessionResource extends AbstractBaseResource {

    @Inject
    private SessionService theService;

    @POST
    @Path("/paged")
    public void getAllPaged(@Suspended final AsyncResponse asyncResponse, final QueryParams request) throws GUSLErrorException {
        resumeAsyncOK(asyncResponse, theService.getAllPaged(request));
    }

    @POST
    @Path("/paged-query")
    public void getPagedQuery(@Suspended final AsyncResponse asyncResponse, final QueryParams request) throws GUSLErrorException {
        resumeAsyncOK(asyncResponse, theService.getPagedQuery(request));
    }

    @POST
    @Path("/get")
    public void get(@Suspended final AsyncResponse asyncResponse, SessionGetRequestDO request) throws GUSLErrorException {
        resumeAsyncOK(asyncResponse, theService.get(request));
    }

    @POST
    @Path("/get-all-for-user")
    public void getAllForUser(@Suspended final AsyncResponse asyncResponse, SessionSelectAllDO request) throws GUSLErrorException {
        resumeAsyncOK(asyncResponse, theService.getAllForUser(request));
    }

    @POST
    @Path("/status")
    public void changeStatus(@Suspended final AsyncResponse asyncResponse, @Valid SessionStatusChangeRequestDO request) throws GUSLErrorException {
        resumeAsyncOK(asyncResponse, theService.changeStatus(request.getAdminUserEmail(), request.getId(), request.getNewStatus()));
    }

    @POST
    @Path("/query")
    public void query(@Suspended final AsyncResponse asyncResponse, @Valid QueryParams request) throws GUSLErrorException {
        resumeAsyncOK(asyncResponse, theService.query(request));
    }

}
