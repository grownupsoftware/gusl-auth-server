package gusl.auth.api.session;

import gusl.annotations.form.page.PagedDAOResponse;
import gusl.auth.model.login.SessionDataDO;
import gusl.auth.model.session.*;
import gusl.core.exceptions.GUSLErrorException;
import gusl.node.properties.GUSLConfigurable;
import gusl.query.QueryParams;
import org.jvnet.hk2.annotations.Contract;

import java.time.ZonedDateTime;
import java.util.Date;

@Contract
public interface SessionService extends GUSLConfigurable {

    PagedDAOResponse getAllPaged(QueryParams request) throws GUSLErrorException;

    SessionDO get(SessionGetRequestDO request) throws GUSLErrorException;

    CreateSessionResponseDO createSession(CreateSessionRequestDO entity) throws GUSLErrorException;

    CreateSessionResponseDO createSession(
            String userId,
            String username,
            String secretKey,
            SessionDataDO sessionData,
            ZonedDateTime userExpiryDate,
            boolean preSignIn,
            String timezone
    ) throws GUSLErrorException;

    SessionsDO getAllForUser(SessionSelectAllDO request) throws GUSLErrorException;

    SessionDO changeStatus(String adminUser, String id, SessionStatus newStatus) throws GUSLErrorException;

    SessionsDO query(QueryParams request) throws GUSLErrorException;

    void signOut(String id) throws GUSLErrorException;

    String createSignedSessionToken(String userId, String username, SessionDO session);

    String createSignedUserToken(String userId, String username, String secretKey, Date dateCreated, ZonedDateTime expiryDate);

    PagedDAOResponse<SessionDO> getPagedQuery(QueryParams request) throws GUSLErrorException;
}
