package gusl.auth.api.session;

import gusl.annotations.form.page.PagedDAOResponse;
import gusl.auth.errors.AuthenticationErrors;
import gusl.auth.jwt.JwtService;
import gusl.auth.model.device.UtmDO;
import gusl.auth.model.login.SessionDataDO;
import gusl.auth.model.session.*;
import gusl.auth.model.token.SessionTokenDO;
import gusl.auth.model.token.UserTokenDO;
import gusl.auth.repository.SessionDAO;
import gusl.core.config.ConfigUtils;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.exceptions.GUSLException;
import gusl.core.utils.IdGenerator;
import gusl.model.cache.CacheAction;
import gusl.model.nodeconfig.NodeConfig;
import gusl.model.nodeconfig.SessionConfig;
import gusl.node.service.AbstractNodeService;
import gusl.query.QueryParams;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import static gusl.auth.model.session.SessionDO.DEFAULT_SESSION_TIMEOUT;
import static java.util.Objects.isNull;

@Service
public class SessionServiceImpl extends AbstractNodeService implements SessionService {

    @Inject
    private SessionDAO theSessionDAO;

    @Inject
    private JwtService theJwtService;

    private SessionConfig theSessionConfig;
    private final SessionStatusTransitionValidator theSessionStatusTransitionValidator = new SessionStatusTransitionValidator();
    long sessionTimeoutAsMilli = DEFAULT_SESSION_TIMEOUT;

    @Override
    public PagedDAOResponse getAllPaged(final QueryParams request) throws GUSLErrorException {
        return theSessionDAO.getAllPaged(request);
    }

    @Override
    public CreateSessionResponseDO createSession(CreateSessionRequestDO entity) throws GUSLErrorException {
        return createSession(
                entity.getUserId(),
                entity.getUsername(),
                entity.getSecretKey(),
                entity,
                entity.getUserExpiryDate(),
                entity.isPreSignIn(),
                entity.getTimezone());
    }

    public CreateSessionResponseDO createSession(
            String userId,
            String username,
            String secretKey,
            SessionDataDO sessionData,
            ZonedDateTime userExpiryDate,
            boolean preSignIn,
            String timezone
    ) throws GUSLErrorException {

        logger.info("createSession: {} {} {} {} ", userId, username, secretKey, userExpiryDate);

        ZonedDateTime now = ZonedDateTime.now(ZoneId.of("UTC"));
        ZonedDateTime expiryTime = now.plus(sessionTimeoutAsMilli, ChronoUnit.MILLIS);

        final SessionDO session = SessionDO.builder()
                .id(IdGenerator.generateUniqueNodeIdAsString())
                .dateCreated(now)
                .userId(userId)
                .username(username)
                .deviceInfo(sessionData.getDeviceInfo())
                .utm(sanitiseUTM(sessionData.getUtm()))
                .deviceInfo(sessionData.getDeviceInfo())
                .clientIp(sessionData.getClientIp())
                .ipLocationData(sessionData.getIpLocationData())
                .deviceLocationData(sessionData.getDeviceLocationData())
                .headers(sessionData.getHeaders())
                .status(SessionStatus.ACTIVE)
                .preSignIn(preSignIn)
                .timezone(timezone)
                .build();

        session.setExpiryDate(expiryTime);
        session.setSessionToken(createSignedSessionToken(userId, username, session));
        session.setUserToken(createSignedUserToken(userId, username, secretKey, new Date(), userExpiryDate));

        theSessionDAO.insert(session);
        broadcastEvent(new SessionCacheEvent(CacheAction.ADD, session));

        return CreateSessionResponseDO.builder().id(userId).session(session).build();
    }

    @Override
    public void signOut(String id) throws GUSLErrorException {
        SessionDO latest = theSessionDAO.updateStatus(id, SessionStatus.LOGGED_OUT);
        broadcastEventReportException(new SessionCacheEvent(UPDATE_ACTION, latest));

    }

    private UtmDO sanitiseUTM(UtmDO utm) {
        if (isNull(utm)) {
            return UtmDO.blank();
        }
        return UtmDO.builder().utmCampaign(sanitise(utm.getUtmCampaign())).utmContent(sanitise(utm.getUtmContent())).utmMedium(sanitise(utm.getUtmMedium())).utmOther(sanitise(utm.getUtmOther())).utmSet(sanitise(utm.getUtmSet())).utmSource(sanitise(utm.getUtmSource())).utmTerm(sanitise(utm.getUtmTerm())).utmSet(sanitise(utm.getUtmSet())).build();
    }

    private String sanitise(String value) {
        if (isNull(value)) {
            return null;
        }
        String[] splits = value.split("\\?");
        if (splits.length > 0) {
            return splits[0];
        }
        return value;
    }

    @Override
    public SessionDO get(SessionGetRequestDO request) throws GUSLErrorException {
        return theSessionDAO.findRecord(request.getId());
    }

    @Override
    public SessionsDO getAllForUser(SessionSelectAllDO request) throws GUSLErrorException {
        return SessionsDO.builder().sessions(theSessionDAO.getAllForUser(request.getBettorId())).build();
    }

    @Override
    public SessionDO changeStatus(String adminUser, String id, SessionStatus newStatus) throws GUSLErrorException {
        final SessionDO entity = theSessionDAO.findRecord(id);
        if (!theSessionStatusTransitionValidator.isValidTransition(entity.getStatus(), newStatus)) {
            throw new GUSLErrorException(AuthenticationErrors.INVALID_STATUS_CHANGE.getError(entity.getStatus().name(), newStatus.name()));
        }
        SessionDO update = theSessionDAO.updateStatus(id, newStatus);
        logger.audit("Change in session status: {} {} {} {}", update.getId(), update.getUserId(), update.getUsername(), update.getStatus());
        broadcastEventReportException(new SessionCacheEvent(UPDATE_ACTION, update));
        return update;
    }

    @Override
    public SessionsDO query(QueryParams request) throws GUSLErrorException {
        return SessionsDO.builder().sessions(theSessionDAO.get(request)).build();
    }

    @Override
    public void configure(NodeConfig config) throws GUSLException {
        theSessionConfig = config.getSessionConfig();
        if (isNull(theSessionConfig)) {
            logger.error("Session config is missing from application.json");
            throw new GUSLException("Session config is missing from application.json");
        }

        try {
            sessionTimeoutAsMilli = ConfigUtils.millisNumberResolver(theSessionConfig.getSessionTimeout());
        } catch (GUSLException ex) {
            logger.error("Session config Session Timeout incorrect", ex);
            throw new GUSLException("Session config Session Timeout incorrect");
        }

        logger.info("Session timeout set to {} minutes", (sessionTimeoutAsMilli / 1000 / 60));
    }

    public static Date asDate(ZonedDateTime dateTime) {
        return Date.from(dateTime.toInstant());
    }

    public String createSignedSessionToken(String userId, String username, SessionDO session) {
        final SessionTokenDO token = SessionTokenDO.builder()
                .sessionId(session.getId())
                .id(userId)
                .username(username)
                .createdDate(asDate(session.getDateCreated()))
                .expiryDate(asDate(session.getExpiryDate()))
                .build();
        return theJwtService.signClaims(theJwtService.getSessionSecurityKey(), token.getClaims());
    }

    public String createSignedUserToken(String userId, String username, String secretKey, Date dateCreated, ZonedDateTime expiryDate) {
        final UserTokenDO token = UserTokenDO.builder()
                .id(userId)
                .customer(true)
                .secretKey(secretKey)
                .username(username)
                .createdDate(dateCreated)
                .expiryDate(asDate(expiryDate))
                .build();
        return theJwtService.signClaims(theJwtService.getCustomerSecurityKey(), token.getClaims());
    }

    @Override
    public PagedDAOResponse<SessionDO> getPagedQuery(QueryParams queryParams) throws GUSLErrorException {
        return theSessionDAO.getAllPaged(queryParams);
    }

}
