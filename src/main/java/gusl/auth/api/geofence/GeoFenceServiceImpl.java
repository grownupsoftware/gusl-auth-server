package gusl.auth.api.geofence;

import gusl.annotations.form.page.PageRequest;
import gusl.annotations.form.page.PagedDAOResponse;
import gusl.auth.model.geofence.GeoFenceDO;
import gusl.auth.model.geofence.GeoFencesDO;
import gusl.auth.model.geofence.PagedGeoFenceDO;
import gusl.auth.model.roles.RoleCacheEvent;
import gusl.auth.repository.GeoFenceDAO;
import gusl.core.exceptions.GUSLErrorException;
import gusl.model.cache.CacheAction;
import gusl.model.utils.SimpleStringRequestDTO;
import gusl.query.QueryParams;
import lombok.CustomLog;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;

@CustomLog
@Service
public class GeoFenceServiceImpl implements GeoFenceService {

    @Inject
    private GeoFenceDAO theGeoFenceDAO;

    @Override
    public PagedDAOResponse<GeoFenceDO> getPagedQuery(QueryParams queryParams) throws GUSLErrorException {
        return theGeoFenceDAO.getAllPaged(queryParams);
    }

    @Override
    public GeoFencesDO getAll() throws GUSLErrorException {
        return GeoFencesDO.builder()
                .geoFences(theGeoFenceDAO.getAll())
                .build();
    }

    @Override
    public GeoFenceDO getActive() throws GUSLErrorException {
//        SystemConfig config = theSystemConfigCache.get(SystemConfigDO.DEFAULT_ID);
//        return getGeoFence(config.getProperties().getGeoFenceId());
        //logger.info("-------- fix me - should use system config");
        // cannot use SystemConfigCache - ideally should get rid of getActive and use get(request)
        // and let further up the food chain decide what is active
        return theGeoFenceDAO.findRecord("1");
    }

    @Override
    public GeoFenceDO insert(String adminUserEmail, GeoFenceDO contents) throws GUSLErrorException {
        theGeoFenceDAO.insert(contents);
        return contents;
    }

    @Override
    public GeoFenceDO get(SimpleStringRequestDTO requestDo) throws GUSLErrorException {
        return theGeoFenceDAO.findRecord(requestDo.getId());
    }

    @Override
    public PagedGeoFenceDO getAllPaged(PageRequest request) throws GUSLErrorException {
        PagedGeoFenceDO result = new PagedGeoFenceDO();
        result.setContent(theGeoFenceDAO.getAll());
        return result;
    }

    @Override
    public GeoFenceDO update(String adminUserEmail, GeoFenceDO contents) throws GUSLErrorException {
        return theGeoFenceDAO.update(contents);
    }
}
