package gusl.auth.api.geofence;

import gusl.annotations.form.page.PageRequest;
import gusl.annotations.form.page.PagedDAOResponse;
import gusl.auth.model.geofence.GeoFenceDO;
import gusl.auth.model.geofence.GeoFencesDO;
import gusl.auth.model.geofence.PagedGeoFenceDO;
import gusl.auth.model.session.SessionDO;
import gusl.core.exceptions.GUSLErrorException;
import gusl.model.utils.SimpleStringRequestDTO;
import gusl.query.QueryParams;
import org.jvnet.hk2.annotations.Contract;

@Contract
public interface GeoFenceService {

    GeoFencesDO getAll() throws GUSLErrorException;

    GeoFenceDO getActive() throws GUSLErrorException;

    GeoFenceDO insert(String username, GeoFenceDO contents) throws GUSLErrorException;

    GeoFenceDO get(SimpleStringRequestDTO requestDo) throws GUSLErrorException;

    PagedGeoFenceDO getAllPaged(PageRequest request) throws GUSLErrorException;

    GeoFenceDO update(String username, GeoFenceDO contents) throws GUSLErrorException;

    PagedDAOResponse<GeoFenceDO> getPagedQuery(QueryParams request) throws GUSLErrorException;

}
