package gusl.auth.api.geofence;

import gusl.annotations.form.page.PageRequest;
import gusl.auth.model.geofence.GeoFenceDO;
import gusl.core.exceptions.GUSLErrorException;
import gusl.model.dataservice.dtos.AuditableDTO;
import gusl.model.utils.SimpleStringRequestDTO;
import gusl.node.resources.AbstractBaseResource;
import gusl.query.QueryParams;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.validation.Valid;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;

import static gusl.node.transport.HttpUtils.resumeAsyncOK;

@Singleton
@Path("geofence/v1")
@PermitAll
public class GeoFenceResource extends AbstractBaseResource {

    @Inject
    private GeoFenceService theService;

    @POST
    @Path("/paged-query")
    public void getPagedQuery(@Suspended final AsyncResponse asyncResponse, final QueryParams request) throws GUSLErrorException {
        resumeAsyncOK(asyncResponse, theService.getPagedQuery(request));
    }

    @POST
    @Path("all")
    public void getAll(@Suspended final AsyncResponse asyncResponse) throws GUSLErrorException {
        resumeAsyncOK(asyncResponse, theService.getAll());
    }

    @POST
    @Path("active")
    public void getActive(@Suspended final AsyncResponse asyncResponse) throws GUSLErrorException {
        resumeAsyncOK(asyncResponse, theService.getActive());
    }

    @POST
    @Path("paged")
    public void getAllPaged(@Suspended final AsyncResponse asyncResponse, final PageRequest request) throws GUSLErrorException {
        resumeAsyncOK(asyncResponse, theService.getAllPaged(request));
    }

    @POST
    @Path("get")
    public void get(@Suspended final AsyncResponse asyncResponse, SimpleStringRequestDTO requestDo) throws GUSLErrorException {
        resumeAsyncOK(asyncResponse, theService.get(requestDo));
    }

    @POST
    @Path("insert")
    public void insert(@Suspended final AsyncResponse asyncResponse, @Valid AuditableDTO<GeoFenceDO> dto) throws GUSLErrorException {
        resumeAsyncOK(asyncResponse, theService.insert(dto.getUsername(), dto.getContents()));
    }

    @POST
    @Path("update")
    public void update(@Suspended final AsyncResponse asyncResponse, @Valid AuditableDTO<GeoFenceDO> dto) throws GUSLErrorException {
        resumeAsyncOK(asyncResponse, theService.update(dto.getUsername(), dto.getContents()));
    }
}
