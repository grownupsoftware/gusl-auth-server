package gusl.auth.api.load;

import gusl.core.exceptions.GUSLErrorException;
import gusl.node.resources.AbstractBaseResource;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;

import static gusl.node.transport.HttpUtils.resumeAsyncOK;

@Singleton
@Path("cache")
@PermitAll
public class LoadResource extends AbstractBaseResource {

    @Inject
    private LoadService theService;

    @POST
    @Path("v1/load/bo")
    public void getForBackoffice(@Suspended final AsyncResponse asyncResponse) throws GUSLErrorException {
        resumeAsyncOK(asyncResponse, theService.getForBackoffice());
    }

    @POST
    @Path("v1/load/fe")
    public void getForFrontend(@Suspended final AsyncResponse asyncResponse) throws GUSLErrorException {
        resumeAsyncOK(asyncResponse, theService.getForFrontend());
    }

}
