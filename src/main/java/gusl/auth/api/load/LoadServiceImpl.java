package gusl.auth.api.load;

import gusl.auth.model.loadcache.LoadBOAuthCacheResponseDO;
import gusl.auth.model.loadcache.LoadFEAuthCacheResponseDO;
import gusl.auth.repository.RoleDAO;
import gusl.core.exceptions.GUSLErrorException;
import lombok.CustomLog;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;

@CustomLog
@Service
public class LoadServiceImpl implements LoadService {

    @Inject
    private RoleDAO theRoleDAO;

    @Override
    public LoadBOAuthCacheResponseDO getForBackoffice() throws GUSLErrorException {
        return LoadBOAuthCacheResponseDO.builder()
                .roles(theRoleDAO.getAll())
                .build();
    }

    @Override
    public LoadFEAuthCacheResponseDO getForFrontend() throws GUSLErrorException {
        return LoadFEAuthCacheResponseDO.builder()
                .roles(theRoleDAO.getAll())
                .build();
    }

}
