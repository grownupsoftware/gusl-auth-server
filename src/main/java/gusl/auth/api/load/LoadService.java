package gusl.auth.api.load;

import gusl.auth.model.loadcache.LoadBOAuthCacheResponseDO;
import gusl.auth.model.loadcache.LoadFEAuthCacheResponseDO;
import gusl.core.exceptions.GUSLErrorException;
import org.jvnet.hk2.annotations.Contract;

@Contract
public interface LoadService {

    LoadBOAuthCacheResponseDO getForBackoffice() throws GUSLErrorException;

    LoadFEAuthCacheResponseDO getForFrontend() throws GUSLErrorException;

}
