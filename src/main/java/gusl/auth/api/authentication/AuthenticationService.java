package gusl.auth.api.authentication;

import gusl.auth.model.login.AuthLoginResponseDO;
import gusl.auth.model.login.GetUserByUsernameRequestDO;
import gusl.auth.model.login.SignInRequestDO;
import gusl.auth.model.register.RegisterRequestDO;
import gusl.auth.model.register.RegisterResponseDO;
import gusl.auth.model.security.*;
import gusl.auth.model.session.CreateSessionRequestDO;
import gusl.auth.model.session.CreateSessionResponseDO;
import gusl.auth.model.session.SessionDO;
import gusl.core.exceptions.GUSLErrorException;
import gusl.model.dataservice.GetStringIdRequestDO;
import gusl.model.dataservice.dtos.AuditableDTO;
import gusl.model.success.SuccessResponseDO;
import org.jvnet.hk2.annotations.Contract;

@Contract
public interface AuthenticationService {
    AuthLoginResponseDO signIn(SignInRequestDO request) throws GUSLErrorException;

    SuccessResponseDO signOut(SessionDO session) throws GUSLErrorException;

    UserSecurityDO insert(UserSecurityDO request) throws GUSLErrorException;

    UserSecurityDO update(AuditableDTO<UserSecurityDO> request) throws GUSLErrorException;

    UserSecurityDO getById(GetStringIdRequestDO request) throws GUSLErrorException;

    UserSecurityDO getByKey(UserSecurityByKeyRequestDO request) throws GUSLErrorException;

    UserSecurityDO getByUser(UserSecurityByUsernameRequestDO request) throws GUSLErrorException;

    RegisterResponseDO register(RegisterRequestDO request) throws GUSLErrorException;

    CreateSessionResponseDO createSession(CreateSessionRequestDO request) throws GUSLErrorException;

    SuccessResponseDO changePassword(ChangePasswordRequestDO request) throws GUSLErrorException;

    SuccessResponseDO changeUsername(ChangeUsernameRequestDO request) throws GUSLErrorException;

    UserSecurityDO getUserByUsername(GetUserByUsernameRequestDO request) throws GUSLErrorException;

    OldSessionTokenResponseDO getOldToken(OldSessionTokenRequestDO request) throws GUSLErrorException;

    UserSecurityDO renewValidationKey(RenewValidationKeyRequestDO request) throws GUSLErrorException;

    UserSecurityDO verifyValidationKey(VerifyValidationKeyRequestDO request) throws GUSLErrorException;
}
