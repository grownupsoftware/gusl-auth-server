package gusl.auth.api.authentication;

import gusl.auth.api.session.SessionService;
import gusl.auth.errors.AuthenticationErrors;
import gusl.auth.jwt.JwtService;
import gusl.auth.model.login.AuthLoginResponseDO;
import gusl.auth.model.login.GetUserByUsernameRequestDO;
import gusl.auth.model.login.SignInRequestDO;
import gusl.auth.model.register.RegisterRequestDO;
import gusl.auth.model.register.RegisterResponseDO;
import gusl.auth.model.security.*;
import gusl.auth.model.session.CreateSessionRequestDO;
import gusl.auth.model.session.CreateSessionResponseDO;
import gusl.auth.model.session.SessionDO;
import gusl.auth.repository.UserSecurityDAO;
import gusl.core.annotations.FixMe;
import gusl.core.config.ConfigUtils;
import gusl.core.dates.DateUtils;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.exceptions.GUSLException;
import gusl.core.security.InvalidHashException;
import gusl.core.security.InvalidOperationException;
import gusl.core.security.ObfuscatedStorage;
import gusl.core.security.PasswordStorage;
import gusl.core.utils.IdGenerator;
import gusl.core.utils.StringUtils;
import gusl.model.dataservice.GetStringIdRequestDO;
import gusl.model.dataservice.dtos.AuditableDTO;
import gusl.model.nodeconfig.NodeConfig;
import gusl.model.nodeconfig.SessionConfig;
import gusl.model.success.SuccessResponseDO;
import gusl.node.properties.GUSLConfigurable;
import gusl.node.service.AbstractNodeService;
import gusl.query.MatchQuery;
import gusl.query.QueryParams;
import io.jsonwebtoken.Claims;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static gusl.auth.errors.AuthenticationErrors.RECORD_NOT_FOUND;
import static gusl.core.utils.Utils.safeStream;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Service
public class AuthenticationServiceImpl extends AbstractNodeService implements AuthenticationService, GUSLConfigurable {

    public static final long DEFAULT_VALIDATION_KEY_DURATION = 300000L;

    @Inject
    private UserSecurityDAO theUserSecurityDAO;

    @Inject
    private SessionService theSessionService;

    @Inject
    private JwtService theJwtService;

    private SessionConfig theSessionConfig;

    long validationKeyDurationAsMilli = DEFAULT_VALIDATION_KEY_DURATION;

    @Override
    public RegisterResponseDO register(RegisterRequestDO request) throws GUSLErrorException {
        logger.warn("Not implemented");
        return RegisterResponseDO.builder().build();
    }

    private UserSecurityDO findByUsername(String userName) throws GUSLErrorException {
        return theUserSecurityDAO.getOne(QueryParams.builder()
                .must(MatchQuery.of("username", userName.toLowerCase()))
                .build()).orElseThrow(() -> {
            logger.info("NF001 Failed to find user: {}", userName.toLowerCase());
            return RECORD_NOT_FOUND.generateException(true, userName.toLowerCase(), "UserSecurity").setSuppressStack(true);
        });
    }

    private UserSecurityDO findByKey(String userName, String secretKey) throws GUSLErrorException {
        String key = ObfuscatedStorage.isObfuscated(secretKey) ? secretKey : ObfuscatedStorage.encrypt(secretKey);
        return theUserSecurityDAO.getOne(QueryParams.builder()
                .must(MatchQuery.of("username", userName.toLowerCase()))
                .must(MatchQuery.of("secretKey", key))
                .build()).orElseThrow(() -> {
            logger.info("NF002 Not found: {} {} ", userName.toLowerCase(), secretKey);
            return RECORD_NOT_FOUND.generateException(true, userName.toLowerCase(), "UserSecurity").setSuppressStack(true);
        });
    }

    @Override
    public AuthLoginResponseDO signIn(SignInRequestDO request) throws GUSLErrorException {
        logger.audit("SignIn attempt {}", request.getUsername());
        final UserSecurityDO userSecurity = findByUsername(request.getUsername());
        try {
            final boolean passwordVerified = PasswordStorage.verifyPassword(request.getPassword(), userSecurity.getPassword());

            if (!passwordVerified) {
                logger.audit("SignIn {} - password did not match", userSecurity.getUsername());
                throw AuthenticationErrors.ERR_LOGIN_FAILURE.generateException(true, userSecurity.getUsername());
            }

            String newSecretKey = updateSecretKey(userSecurity);

            @FixMe(createdBy = "Grant", description = "userExpiryDate s/be aligned with password change period") final CreateSessionResponseDO session = theSessionService.createSession(
                    userSecurity.getId(),
                    userSecurity.getUsername(),
                    newSecretKey,
                    request,
                    ZonedDateTime.now(ZoneId.of("UTC")).plusMonths(1),
                    request.isPreSignIn(),
                    request.getTimezone());

            checkIfScratchCode(request, userSecurity);

            return AuthLoginResponseDO.builder()
                    .id(userSecurity.getId())
                    .session(session.getSession())
                    .build();

        } catch (GUSLErrorException ex) {
            throw ex;
        } catch (InvalidHashException | InvalidOperationException ex) {
            logger.warn("Unable to decrypt password for {}, reason {}", userSecurity.getUsername(), ex.getLocalizedMessage());
            throw AuthenticationErrors.ERR_LOGIN_FAILURE.generateException(true, userSecurity.getUsername());
        } catch (Throwable t) {
            logger.error("Error in signin", t);
            throw AuthenticationErrors.ERR_LOGIN_FAILURE.generateException(true, userSecurity.getUsername());
        }

    }

    private String updateSecretKey(final UserSecurityDO userSecurity) throws GUSLErrorException {
        String newSecretKey = UUID.randomUUID().toString();
        // will be obfuscated on write
        userSecurity.setSecretKey(newSecretKey);
        theUserSecurityDAO.update(userSecurity);
        return ObfuscatedStorage.encrypt(newSecretKey);
    }

    private void checkIfScratchCode(final SignInRequestDO request, final UserSecurityDO userSecurity) throws GUSLErrorException {
        if (StringUtils.isNotBlank(request.getTotp()) && nonNull(userSecurity.getScratchCodes())) {
            safeStream(userSecurity.getScratchCodes().getCodes())
                    .forEach(code -> {
                        try {
                            final String hash = PasswordStorage.createHash(request.getTotp());
                            logger.debug("Scratch code match: {} {} {} {} {} {} ",
                                    request.getTotp(),
                                    ObfuscatedStorage.encrypt(request.getTotp()),
                                    code.getVisibleCode(),
                                    hash,
                                    code.getCode(),
                                    hash.equals(code.getCode()),
                                    ObfuscatedStorage.encrypt(request.getTotp()).equals(code.getVisibleCode()));
                            // vvv or condition is wrmg
                            if (hash.equals(code.getCode()) || ObfuscatedStorage.encrypt(request.getTotp()).equals(code.getVisibleCode())) {
                                code.setUsed(true);
                                logger.audit("SignIn {} - used a scratch code", userSecurity.getUsername());
                                theUserSecurityDAO.update(userSecurity);
                            }
                        } catch (GUSLErrorException e) {
                            logger.error("Error updating scratch code", e);
                        } catch (InvalidOperationException e) {
                            throw new RuntimeException(e);
                        }
                    });
        }
    }

    @Override
    public SuccessResponseDO signOut(SessionDO session) throws GUSLErrorException {
        if (nonNull(session) && nonNull(session.getId())) {
            theSessionService.signOut(session.getId());
        }
        return SuccessResponseDO.success();
    }

    @Override
    public CreateSessionResponseDO createSession(CreateSessionRequestDO request) throws GUSLErrorException {

        logger.info("CreateSessionRequestDO: {}", request);

        final UserSecurityDO bySecretKey = findByKey(request.getUsername(), request.getSecretKey());
        final UserSecurityDO byUsername = findByUsername(request.getUsername());
        validate(request.getUsername(), byUsername, bySecretKey);
        final CreateSessionResponseDO sessionResponse = theSessionService.createSession(
                byUsername.getId(),
                byUsername.getUsername(),
                byUsername.getSecretKey(),
                request,
                ZonedDateTime.now(ZoneId.of("UTC")).plusMonths(1),
                request.isPreSignIn(),
                request.getTimezone());

        return CreateSessionResponseDO.builder()
                .id(byUsername.getId())
                .session(sessionResponse.getSession())
                .build();
    }

    @Override
    public SuccessResponseDO changePassword(ChangePasswordRequestDO request) throws GUSLErrorException {
        UserSecurityDO entity = theUserSecurityDAO.findRecord(request.getId());
        try {
            final boolean passwordVerified = PasswordStorage.isHashed(request.getOldPassword())
                    ? PasswordStorage.verifyHashedPassword(request.getOldPassword(), entity.getPassword())
                    : PasswordStorage.verifyPassword(request.getOldPassword(), entity.getPassword());
            if (!passwordVerified) {
                throw AuthenticationErrors.INVALID_PASSWORD.generateException("Password", "Do not match");
            }
            entity.setPassword(PasswordStorage.createHash(request.getNewPassword()));
        } catch (InvalidHashException | InvalidOperationException ex) {
            throw AuthenticationErrors.FAILED_TO_HASH_PASSWORD.generateException(true, entity.getUsername());
        }
        theUserSecurityDAO.update(entity);
        return SuccessResponseDO.success();
    }

    @Override
    public SuccessResponseDO changeUsername(ChangeUsernameRequestDO request) throws GUSLErrorException {
        final UserSecurityDO userSecurity = findByUsername(request.getOldUsername());
        final UserSecurityDO newSecurity = convert(userSecurity, UserSecurityDO.class);
        newSecurity.setUsername(request.getNewUsername().toLowerCase());

        if (usernameExists(newSecurity.getUsername())) {
            throw AuthenticationErrors.USERNAME_EXISTS.generateException(true, newSecurity.getUsername());
        }
        newSecurity.setSecretKey(IdGenerator.getUUID());

        theUserSecurityDAO.update(newSecurity);
        return SuccessResponseDO.success();
    }

    @Override
    public UserSecurityDO getUserByUsername(GetUserByUsernameRequestDO request) throws GUSLErrorException {
        return findByUsername(request.getUsername());
    }

    private void validate(String username, final UserSecurityDO byUsername, final UserSecurityDO bySecretKey) throws GUSLErrorException {
        if (isNull(byUsername) || isNull(bySecretKey)) {
            logger.warn("WRN001 byUsername: isNull={}  bySecretKey: isNull={}", isNull(byUsername), isNull(bySecretKey));
            throw AuthenticationErrors.ERR_LOGIN_FAILURE.generateException(true, username);
        }

        if (!byUsername.getId().equals(bySecretKey.getId())) {
            // diff user
            logger.warn("WRN002 byUsername: id={}  bySecretKey: id={}", byUsername.getId(), bySecretKey.getId());
            throw AuthenticationErrors.ERR_LOGIN_FAILURE.generateException(true, username);
        }

        if (!byUsername.getUsername().equalsIgnoreCase(username)) {
            // diff user
            logger.warn("WRN003 byUsername: [{}]  !=  username: [{}]", byUsername.getUsername(), username);
            throw AuthenticationErrors.ERR_LOGIN_FAILURE.generateException(true, username);
        }
    }

    private boolean usernameExists(String username) {
        try {
            findByUsername(username);
            logger.info("Username: {} found: true", username);
            return true;
        } catch (GUSLErrorException e) {
            return false;
        }
    }

    @Override
    public UserSecurityDO insert(UserSecurityDO entity) throws GUSLErrorException {
        entity.setUsername(entity.getUsername().toLowerCase());
        if (usernameExists(entity.getUsername())) {
            throw AuthenticationErrors.USERNAME_EXISTS.generateException(true, entity.getUsername());
        }

        entity.setSecretKey(IdGenerator.getUUID());
        try {
            entity.setPassword(PasswordStorage.createHash(entity.getPassword()));
        } catch (InvalidOperationException e) {
            throw AuthenticationErrors.FAILED_TO_HASH_PASSWORD.generateException(true, entity.getUsername());
        }

        entity.setValidationKey(createValidationKey(entity));
        theUserSecurityDAO.insert(entity);
        return entity;
    }

    private String createValidationKey(UserSecurityDO userSecurity) {
        ZonedDateTime now = ZonedDateTime.now(ZoneId.of("UTC"));
        ZonedDateTime expiryTime = now.plus(validationKeyDurationAsMilli, ChronoUnit.MILLIS);

        final ValidationKeyTokenDO key = ValidationKeyTokenDO.builder()
                .createdDate(new Date())
                .id(IdGenerator.generateUniqueNodeIdAsString())
                .expiryDate(DateUtils.asDate(expiryTime))
                .email(userSecurity.getUsername())
                .userSecurityId(userSecurity.getId())
                .userSecretKey(userSecurity.getSecretKey())
                .build();
        return theJwtService.signClaims(theJwtService.getAnonSecurityKey(), key.getClaims());
    }

    @Override
    public UserSecurityDO renewValidationKey(RenewValidationKeyRequestDO request) throws GUSLErrorException {
        final UserSecurityDO userSecurity = theUserSecurityDAO.findRecord(request.getId());
        if (isNull(userSecurity)) {
            throw AuthenticationErrors.USER_NOT_FOUND.generateException(true, request.getId());
        }
        userSecurity.setValidationKey(createValidationKey(userSecurity));
        return theUserSecurityDAO.update(userSecurity);
    }

    @Override
    public UserSecurityDO verifyValidationKey(VerifyValidationKeyRequestDO request) throws GUSLErrorException {
        if (isNull(request) || StringUtils.isBlank(request.getToken())) {
            throw AuthenticationErrors.INVALID_VALUE.generateException("data", "is empty");
        }

        try {
            final List<UserSecurityDO> validationKeys = theUserSecurityDAO.get(QueryParams.builder()
                    .must(MatchQuery.of("validationKey", request.getToken()))
                    .build());

            if (validationKeys.size() != 1) {
                logger.error("Found 0 or more than 1 key. Found: {}", validationKeys.size());
                throw AuthenticationErrors.INVALID_VALUE.generateException("key", "not found");
            }

            final Claims claims = theJwtService.parseToken(theJwtService.getAnonSecurityKey(), request.getToken());
            final ValidationKeyTokenDO validationKeyTokenDO = ValidationKeyTokenDO.parseClaims(claims);
            // an error would be thrown if token (claim) has expired
            final UserSecurityDO userSecurity = theUserSecurityDAO.findRecord(validationKeyTokenDO.getUserSecurityId());
            if (isNull(userSecurity)) {
                logger.error("Failed to find user security id: [{}]", validationKeyTokenDO.getUserSecurityId());
                throw AuthenticationErrors.INVALID_VALUE.generateException("record", "not found");
            }

            if (isNull(userSecurity.getSecretKey()) && !userSecurity.getSecretKey().equals(validationKeyTokenDO.getUserSecretKey())) {
                logger.error("User secret key changed, so token no longer valid");
                throw AuthenticationErrors.INVALID_VALUE.generateException("user detail", "have changed");
            }

            if (!userSecurity.getUsername().equalsIgnoreCase(validationKeyTokenDO.getEmail())) {
                logger.error("Emails are different");
                throw AuthenticationErrors.INVALID_VALUE.generateException("email", "have changed");
            }

            userSecurity.setEmailValidated(true);
            userSecurity.setValidationKey("");
            return theUserSecurityDAO.update(userSecurity);

        } catch (Throwable e) {
            logger.error("Failed to validate validationKey");
            throw AuthenticationErrors.INVALID_VALUE.generateException("token ", "expired");
        }
    }

    @Override
    public UserSecurityDO update(AuditableDTO<UserSecurityDO> request) throws GUSLErrorException {
        final UserSecurityDO userSecurity = theUserSecurityDAO.findRecord(request.getContents().getId());
        if (isNull(userSecurity)) {
            throw AuthenticationErrors.USER_NOT_FOUND.generateException(true, request.getContents().getId());
        }
        if (nonNull(request.getContents().getSecretKey())) {
            userSecurity.setSecretKey(request.getContents().getSecretKey());
        }
        if (nonNull(request.getContents().getRoleIds())) {
            userSecurity.setRoleIds(request.getContents().getRoleIds());
        }
        if (nonNull(request.getContents().getAuthenticatorKey())) {
            userSecurity.setAuthenticatorKey(request.getContents().getAuthenticatorKey());
        }
        if (nonNull(request.getContents().getScratchCodes())) {
            userSecurity.setScratchCodes(request.getContents().getScratchCodes());
        }
        return theUserSecurityDAO.update(request.getContents());
    }

    @Override
    public UserSecurityDO getById(GetStringIdRequestDO request) throws GUSLErrorException {
        return theUserSecurityDAO.findRecord(request.getId());
    }

    @Override
    public UserSecurityDO getByKey(UserSecurityByKeyRequestDO request) throws GUSLErrorException {
        return findByKey(request.getUsername(), request.getSecretKey());
    }

    @Override
    public UserSecurityDO getByUser(UserSecurityByUsernameRequestDO request) throws GUSLErrorException {
        return findByUsername(request.getUsername());
    }

    @Override
    public OldSessionTokenResponseDO getOldToken(OldSessionTokenRequestDO request) throws GUSLErrorException {

        /*
            String createSignedSessionToken(String userId, String username, SessionDO session);

    String createSignedUserToken(String userId, String username, String secretKey, Date dateCreated, ZonedDateTime expiryDate);

         */
        theSessionService.createSignedSessionToken(
                request.getUserId(),
                request.getUsername(),
                SessionDO.builder()
                        .username(request.getUsername())
                        .dateCreated(request.getDateCreated())
                        .expiryDate(request.getExpiryDate())
                        .build());

        return OldSessionTokenResponseDO.builder()
                .oldUserToken(theSessionService.createSignedUserToken(
                        request.getUserId(),
                        request.getUsername(),
                        request.getSecretKey(),
                        DateUtils.asDate(request.getDateCreated()),
                        request.getExpiryDate()))
                .oldSessionToken(theSessionService.createSignedSessionToken(
                        request.getUserId(),
                        request.getUsername(),
                        SessionDO.builder()
                                .username(request.getUsername())
                                .dateCreated(request.getDateCreated())
                                .expiryDate(request.getExpiryDate())
                                .build()))
                .build();
    }

    @Override
    public void configure(NodeConfig config) throws GUSLException {
        theSessionConfig = config.getSessionConfig();
        if (isNull(theSessionConfig)) {
            logger.error("Session config is missing from application.json");
            throw new GUSLException("Session config is missing from application.json");
        }

        try {
            if (theSessionConfig.getValidationLinkDuration() != null) {
                validationKeyDurationAsMilli = ConfigUtils.millisNumberResolver(theSessionConfig.getValidationLinkDuration());
            }
        } catch (GUSLException ex) {
            logger.error("Validation Key Duration incorrect", ex);
            throw new GUSLException("Validation Key Duration ");
        }

        logger.info("Validation key duration set to {} minutes", (validationKeyDurationAsMilli / 1000 / 60));
    }

}
