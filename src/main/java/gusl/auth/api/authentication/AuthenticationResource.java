package gusl.auth.api.authentication;

import gusl.auth.model.login.AuthLoginResponseDO;
import gusl.auth.model.login.GetUserByUsernameRequestDO;
import gusl.auth.model.login.SignInRequestDO;
import gusl.auth.model.register.RegisterRequestDO;
import gusl.auth.model.security.*;
import gusl.auth.model.session.CreateSessionRequestDO;
import gusl.auth.model.session.SessionDO;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.json.JsonUtils;
import gusl.model.dataservice.GetStringIdRequestDO;
import gusl.model.dataservice.dtos.AuditableDTO;
import gusl.node.resources.AbstractBaseResource;
import gusl.node.transport.HttpUtils;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;

import static gusl.node.transport.HttpUtils.resumeAsyncOK;

@Singleton
@Path("auth/v1")
@PermitAll
public class AuthenticationResource extends AbstractBaseResource {

    @Inject
    protected AuthenticationService theService;

    @POST
    @Path("register")
    public void register(@Suspended final AsyncResponse asyncResponse, RegisterRequestDO request) throws GUSLErrorException {
        resumeAsyncOK(asyncResponse, theService.register(request));
    }

    @POST
    @Path("login")
    public void signIn(@Suspended final AsyncResponse asyncResponse, SignInRequestDO request) throws GUSLErrorException {
        try {
            logger.debug("login: {}", JsonUtils.prettyPrint(request));
            final AuthLoginResponseDO response = theService.signIn(request);
            logger.debug("response: {}", JsonUtils.prettyPrint(response));
            resumeAsyncOK(asyncResponse, response);
        } catch (Throwable t) {
            logger.error("error", t);
            HttpUtils.resumeAsyncError(asyncResponse, t);
        }
    }

    @POST
    @Path("logout")
    public void signOut(@Suspended final AsyncResponse asyncResponse, SessionDO session) throws GUSLErrorException {
        resumeAsyncOK(asyncResponse, theService.signOut(session));
    }

    @POST
    @Path("change-password")
    public void changePassword(@Suspended final AsyncResponse asyncResponse, ChangePasswordRequestDO request) throws GUSLErrorException {
        resumeAsyncOK(asyncResponse, theService.changePassword(request));
    }

    @POST
    @Path("change-username")
    public void changeUsername(@Suspended final AsyncResponse asyncResponse, ChangeUsernameRequestDO request) throws GUSLErrorException {
        resumeAsyncOK(asyncResponse, theService.changeUsername(request));
    }

    @POST
    @Path("create-session")
    public void createSession(@Suspended final AsyncResponse asyncResponse, CreateSessionRequestDO request) throws GUSLErrorException {
        resumeAsyncOK(asyncResponse, theService.createSession(request));
    }

    @POST
    @Path("insert")
    public void insert(@Suspended final AsyncResponse asyncResponse, UserSecurityDO request) throws GUSLErrorException {
        resumeAsyncOK(asyncResponse, theService.insert(request));
    }

    @POST
    @Path("update")
    public void update(@Suspended final AsyncResponse asyncResponse, AuditableDTO<UserSecurityDO> request) throws GUSLErrorException {
        resumeAsyncOK(asyncResponse, theService.update(request));
    }

    @POST
    @Path("get-by-key")
    public void getByKey(@Suspended final AsyncResponse asyncResponse, UserSecurityByKeyRequestDO request) throws GUSLErrorException {
        resumeAsyncOK(asyncResponse, theService.getByKey(request));
    }

    @POST
    @Path("get-by-user")
    public void getByUser(@Suspended final AsyncResponse asyncResponse, UserSecurityByUsernameRequestDO request) throws GUSLErrorException {
        resumeAsyncOK(asyncResponse, theService.getByUser(request));
    }

    @POST
    @Path("get-by-username")
    public void getUserByUsername(@Suspended final AsyncResponse asyncResponse, GetUserByUsernameRequestDO request) throws GUSLErrorException {
        resumeAsyncOK(asyncResponse, theService.getUserByUsername(request));
    }

    @POST
    @Path("get-by-id")
    public void getById(@Suspended final AsyncResponse asyncResponse, GetStringIdRequestDO request) throws GUSLErrorException {
        resumeAsyncOK(asyncResponse, theService.getById(request));
    }

    @POST
    @Path("old-token")
    public void getOldToken(@Suspended final AsyncResponse asyncResponse, OldSessionTokenRequestDO request) throws GUSLErrorException {
        resumeAsyncOK(asyncResponse, theService.getOldToken(request));
    }

    @POST
    @Path("renew-validation-key")
    public void renewValidationKey(@Suspended final AsyncResponse asyncResponse, RenewValidationKeyRequestDO request) throws GUSLErrorException {
        resumeAsyncOK(asyncResponse, theService.renewValidationKey(request));
    }

    @POST
    @Path("verify-validation-key")
    public void verifyValidationKey(@Suspended final AsyncResponse asyncResponse, VerifyValidationKeyRequestDO request) throws GUSLErrorException {
        resumeAsyncOK(asyncResponse, theService.verifyValidationKey(request));
    }

}
